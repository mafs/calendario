import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { UserI } from '../interfaces/interfaces';
import { Router } from '@angular/router';

function passwordMatch( c:AbstractControl ): { [key: string]: boolean } | null {
  let pass = c.get('pass');
  let pass2 = c.get('pass2');
  if( pass.pristine || pass2.pristine  ) return null;
  if(pass.value === pass2.value){
    return null;
  } else {
    return { 'igual': true };
  }
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formulario: FormGroup;
  formularioLogin: FormGroup;
  constructor(
    private UserServices: UserService,
    private fb: FormBuilder,
    private router: Router
    ) { }

  ngOnInit() {
    this.crearFormulario();
    this.crearFormularioLogin();
  }

  crearFormulario(): void {
    this.formulario = this.fb.group({
      user: ['', Validators.required],
      passwordGroup: this.fb.group({
        pass: ['', Validators.required],
        pass2: ['', Validators.required]
      }, { 'validator': passwordMatch })
    });
  }

  crearFormularioLogin(): void {
    this.formularioLogin = this.fb.group({
      user: ['', Validators.required],
      pass: ['', Validators.required]
    });
  }

  registrarte(){
    // console.log(this.formulario.value);
    this.UserServices.register(this.formulario.value).subscribe(data => console.log(data),
    err => console.log(err));
    this.formulario.setValue({user: '', passwordGroup: {pass: '', pass2: ''}});
  }

  login(){
    // console.log( this.formularioLogin.value );
    this.UserServices.login( this.formularioLogin.value ).subscribe(data => {
      this.router.navigate(['/inicio/calendario']);
      localStorage.setItem('usuario', JSON.stringify({usuario: this.formularioLogin.get('user').value, tipo: 'admin'}));
      this.formularioLogin.patchValue({user: '', pass: ''});
    });
  }
  

}
