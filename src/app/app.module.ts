import { NgModule } from '@angular/core';

// import { CommonModule } from '@angular/common';
//sirve para las peticiones http
import { HttpClientModule } from '@angular/common/http';

// import {NgForm} from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

//modulo 
import { PagesModule } from './pages/pages.module';

//rutas
import { APP_ROUTES_ROOT } from './app.routes';


//esto es para el material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ErrorPages404Component } from './error-pages404/error-pages404.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ErrorPages404Component
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    APP_ROUTES_ROOT,
    PagesModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
