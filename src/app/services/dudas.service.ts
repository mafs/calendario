import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { dudaI } from '../interfaces/interfaces';
import { SERVER } from 'src/environments/environment';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DudasService {

  constructor(private http: HttpClient) { }

  getAllDudas(): Observable<dudaI[]> {
    return this.http.post<dudaI[]>(` ${SERVER}/dudas `, {desde: 0});
  }

  eliminarDuda(id: string): Observable<any>{
    return this.http.delete<any>(  ` ${SERVER}/dudas/${id} ` );
  }

  agregarDuda(id: string, pregunta: any): Observable<any>{
    return this.http.post<any>(`${SERVER}/dudas`, {pregunta});
  }

  actualizarDuda(id: string, comentario: string): Observable<any>{
    console.log(id, comentario);
    return this.http.put<any>(` ${ SERVER }/dudas/${id}/actualizar `, {comentario});
  }

  paginacion(paginacion: number):Observable<any[]>{
    console.log(paginacion, typeof(paginacion));
    
    return this.http.post<any[]>( `${SERVER}/dudas`, {desde:paginacion});
  }

}
