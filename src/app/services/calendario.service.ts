import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { SERVER } from "../../environments/environment";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CalendarioService {

  constructor(private http: HttpClient) { }

  refresh: Subject<any> = new Subject();
  getDates(): Observable<any> {
    return this.http.get<any>(`${SERVER}/fecha`).pipe(map(data => {
      return data.fecha;
    }));
  }

  postDate(): Observable<any>{
    // #ad2121
    const data = {title:'Nuevo Evento', color: '#ad2121'};
    return this.http.post<any>(`${SERVER}/fecha`, data);
  }

  fechaInicial( id:string, inicial: boolean, fecha: Date ) :Observable<any> {
    console.log(id, inicial, fecha);
    const fechaActual: any = { 
      year: fecha.getFullYear(),
      mes:fecha.getMonth(),
      dia: fecha.getDate()
    }    
    if(inicial){
      return this.http.put<any>(`${SERVER}/fecha/${id}/${inicial}`, fechaActual);
    } else {
      return this.http.put<any>(`${SERVER}/fecha/${id}/${inicial}`, fechaActual);
    }
  }

  cambiarNombre(id: string, nombre: string): Observable<any> {
    return this.http.put<any>(` ${SERVER}/fecha/nombre/${id}/${nombre}`, {});
  }

  cambiarColor(color: string, id: string): Observable<any>{
    color = color.split('#')[1];
    return this.http.put<any>(`${SERVER}/fecha/color/${id}/${color}/colors`, {});
  }
}
