import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER } from 'src/environments/environment';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(data: any): Observable<any>{
    return this.http.post<any>( ` ${SERVER}/user/login `, data);
  }

  register(data: any): Observable<any>{
    return this.http.post<any>( ` ${SERVER}/user`, data);
  }
}
