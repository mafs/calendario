import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ErrorPages404Component } from './error-pages404/error-pages404.component';

const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '', redirectTo: '/inicio', pathMatch: 'full' },
    { path: '**', component: ErrorPages404Component }
];

export const APP_ROUTES_ROOT = RouterModule.forRoot(APP_ROUTES);