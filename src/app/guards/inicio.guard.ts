import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class InicioGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate() {
    const usuario = JSON.parse(localStorage.getItem('usuario'));
    if(!usuario){
      this.router.navigate(['/login']);
      console.log(false, ' no debe de entrar');
      return false;
    } else {
      console.log(true, 'entro');
      return true;
    }
  }
}
