import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject, fromEvent, timer } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import { CalendarioService } from 'src/app/services/calendario.service';
import { FormControl } from "@angular/forms";
import { map, debounceTime } from 'rxjs/operators';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styles: [`.color{ background: white }`]
})
export class CalendarioComponent implements OnInit {

  isAdmin = false;
  date = new FormControl(new Date());
  fechaFinal = new FormControl(new Date());

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();
  constructor( private FechasServices: CalendarioService ) {}
  
  events: CalendarEvent[] = [
    // {
    //   start: subDays(startOfDay(new Date()), 1),
    //   end: addDays(new Date(), 1),
    //   title: 'A 3 day event',
    //   color: colors.red,
    //   allDay: true,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true
    //   },
    //   draggable: true
    // }
  ];

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date)) ||
        events.length === 0
      ) {

      } else {
      }
    }
  }

  eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
  }

  addEvent(): void {  
    this.FechasServices.postDate().subscribe(data => {
      // console.log(data.fecha);
      
      this.events = [
        ...this.events, {
          id: data.fecha._id,
          title: 'Nuevo evento',
          start: startOfDay(data.fecha.start),
          end: endOfDay(data.fecha.end),
          color: colors.red,
          draggable: true,
          allDay: true,
          resizable: {
            beforeStart: true,
            afterEnd: true
          }
        }
      ]
    });
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  next(color: string, id: string ){
    this.FechasServices.cambiarColor(color, id).subscribe(data => console.log(data));
  }
  prueba(i: number, id: string) {
    
    
    const input = document.getElementsByClassName('aver')[i];
    fromEvent(input, 'keyup').pipe( debounceTime(1000))
      .subscribe((data: any) => {
        console.log(data.target.value);
        this.FechasServices.cambiarNombre(id, data.target.value).subscribe(data => console.log(data));
      });
  }

  recibeFecha( evento: any, fecha: any, id:string ){
    // console.log(fecha.value);
    
    this.events = this.events.map( event => {
      if(event == evento){
        event.start = fecha.value;
        event.end = fecha.value;
      }
      return event;
    });
    console.log(evento.id);
    
    this.FechasServices.fechaInicial(evento.id, true, fecha.value).subscribe(data => console.log(data) );
  }

  recibeFechaFinal( evento: any, fecha: any ){
    console.log(evento.id);
    
    this.events = this.events.map( event => {
      if(event == evento){
        event.end = fecha.value;
      }
      return event;
    });
    this.FechasServices.fechaInicial(evento.id, false, fecha.value).subscribe(data => console.log(data) );
    
  }

  ngOnInit() {
    this.getEventos();
    const usuario = JSON.parse( localStorage.getItem('usuario') );
    if(usuario.usuario.length > 1){
      this.isAdmin = true;
    }
  }
  getEventos(): void {
    this.FechasServices.getDates().subscribe(data => {
      data.map(data => {       
        this.events = [
          ...this.events,
          {
            id: data._id,
          title: data.title,
          start: startOfDay(data.start),
          end: endOfDay(data.end),
          color: data.color.red,
          draggable: true,
          allDay: true,
          resizable: {
            beforeStart: true,
            afterEnd: true
          }
        }

        ]

      });
    });
  }
}
