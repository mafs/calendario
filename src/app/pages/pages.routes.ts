import { RouterModule, Routes } from '@angular/router'

import { InicioComponent } from './inicio/inicio.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { PostComponent } from './post/post.component';
import { InicioGuard } from '../guards/inicio.guard';

const pages: Routes = [
    { 
        path: 'inicio',
        component: InicioComponent,
        // canActivate: [InicioGuard],
        children: [
            { path: 'calendario', canActivate:[InicioGuard], component: CalendarioComponent, data: { titulo: 'calendario' }},
            { path: 'dudas', component: PostComponent, data: { titulo: 'Dudas' }}
        ]
    },
    
];

export const pages_routes = RouterModule.forChild(pages);