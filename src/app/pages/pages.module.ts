import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//sirve paa en ngModel
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//este es el modulo de los estilos con angular material
import { MaterialModule } from '../material/material.module';

//rutas
import { pages_routes } from './pages.routes';

//componentes
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { InicioComponent } from './inicio/inicio.component';
import { CalendarioComponent } from './calendario/calendario.component';

//calendario
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { PostComponent } from './post/post.component';

@NgModule({
  imports: [
    CommonModule,
    pages_routes,
    FormsModule,
    MaterialModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ReactiveFormsModule
  ],
  declarations: [NavBarComponent, InicioComponent, CalendarioComponent, PostComponent]
})
export class PagesModule { }
