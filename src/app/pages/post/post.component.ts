import { Component, OnInit, ViewContainerRef, ViewChild, Renderer2, ElementRef, OnDestroy } from '@angular/core';
import { DudasService } from 'src/app/services/dudas.service';
import { dudaI } from 'src/app/interfaces/interfaces';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from "@angular/forms";
// import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: [`post.component.css`]
})
export class PostComponent implements OnInit, OnDestroy {
  @ViewChild('uno') uno;
  formularioDuda = new FormGroup({
    mensaje : new FormControl('', [Validators.required, Validators.minLength(8)])
  });

  nuevoFormulario = this.fb.group({
    dudas: this.fb.array([])
  });
  boton = 'cargar comentario';
  disabled = false;
  paginacion = 0;
  mensaje: any = new FormControl('');
  mostarModal = true;
  total: number;
  dudas: dudaI[];
  idDuda = '';
  idmia: any = 1;
  idParaBuscar = '';
  x: dudaI;
  constructor(
    private dudaServices: DudasService,
    private rendere: Renderer2,
    private el: ElementRef,
    private fb: FormBuilder
    ) { }

  ngOnInit() {
    this.obtenerDudas();    
  }

  duda(): FormGroup{
    return this.fb.group({
      duda: ['', [Validators.required, Validators.minLength(4)]]
    })
  }

  get dudasGeter() : FormArray {
    return <FormArray>this.nuevoFormulario.get('dudas');
  }

  agregarDuda(){
    this.dudasGeter.push( this.duda() )
  }

  obtenerDudas(){
    this.dudaServices.getAllDudas().subscribe((data: any) => {
      this.dudas = data.dudas
      this.total = data.total;
    });
  }

  openDialog(id: string, mostrar: boolean) {
    const input = document.getElementById('modaluno');
    mostrar ? input.style.display = 'none' : input.style.display = 'block';
    this.idDuda = id;
  }

  cerrarModal(){
    const input = document.getElementById('modaluno');
    const idMia = this.idDuda;
    input.style.display = 'none';
    this.dudaServices.eliminarDuda(this.idDuda).subscribe(data => console.log(data));
    
    this.dudas = this.dudas.filter(data => data._id !== idMia);
  }

  abrirModal(id: string = '') {
    const input = document.getElementById('modaluno');
    this.mostarModal ? input.style.display = 'none' : input.style.display = 'block';
    this.idDuda = id;
  }
  responder(id: string) {

    console.log(id);
    this.idParaBuscar = id;
    this.dudas.map(data => {
      if(data._id == id){
        const li = this.rendere.createElement('li');
        const input = this.rendere.createElement('input');
        this.rendere.appendChild(li, input);
        this.rendere.addClass(input, 'fff');
        const button = this.rendere.createElement('button');
        this.rendere.setProperty(button, 'id', 'button' + this.idmia);
        this.rendere.setProperty(input, 'id', this.idmia);
        this.rendere.listen( button, 'click', () => this.accederValorInput() );
        const texto = this.rendere.createText('responder');
        this.rendere.appendChild(button, texto);
        this.rendere.addClass(button, 'mt-2');
        const padre = document.getElementById('x'+ data._id);
        this.rendere.appendChild(padre, li);
        this.rendere.appendChild(padre, button);
        
      }

    });

  }
  enviarDuda(){    
    this.dudaServices.agregarDuda('ddd', this.formularioDuda.get('mensaje').value).subscribe(data => {
      console.log(data.dudas);
      const x: dudaI = data.dudas;
      this.formularioDuda.get('mensaje').setValue('');
      this.dudas.push({_id: x._id, usuario: x.usuario, pregunta: x.pregunta});
    });
    
  }

  accederValorInput(){
    const d: any  = document.getElementById(this.idmia);
    // HTMLElement
    // this.dudas.push({_id: x._id, usuario: x.usuario, pregunta: x.pregunta});
    // HTMLHtmlElement
    // HTMLInputElement
    
    this.dudas.map(data => {
      if(data._id == this.idParaBuscar){
        const elemento: HTMLElement = document.getElementById('x'+ data._id);
        const button2: HTMLElement = document.getElementById('button'+ this.idmia);
        this.rendere.removeChild(elemento, d);
        this.rendere.removeChild(elemento, button2);
        const lii = this.rendere.createElement('li');
        const textoo = this.rendere.createText(d.value);
        this.rendere.appendChild(lii, textoo);
        this.rendere.appendChild(elemento, lii);
        this.dudaServices.actualizarDuda( data._id,  d.value).subscribe(data => console.log(data));
      }
    });

    d.value= '';
    this.idmia++;
  }


  getComentario(): void {
    this.paginacion += 10;
    this.dudaServices.paginacion( this.paginacion ).subscribe((data: any) => {
      if(data.dudas.length){
        data.dudas.map(data => this.dudas.push(data));
        console.log(true)
      } else {
        this.boton = 'no hay mas comentarios';
        this.disabled = true;
      }
    });
  }

  ngOnDestroy() {
    this.paginacion = 0;
    this.disabled = false;
  }

}


// this.rendere.listen(button, 'click', () => {
//   padre.childNodes.forEach(data => {
//     if(data == li){
//       console.log('es igual');
//       console.log(data.firstChild);
//     }
//   })   
// });
